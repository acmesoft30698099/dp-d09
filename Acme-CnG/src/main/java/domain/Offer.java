package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;

import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
public class Offer extends Content {
	
	// Constructor ---------------------------
	
	public Offer() {
		super();
	}
	
	// Variables -----------------------------
	
	
	
	// Relations ----------------------------- 
}
