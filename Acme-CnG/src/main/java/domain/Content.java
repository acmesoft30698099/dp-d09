package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Content extends CommentableEntity {
	
	// Constructor -----------------------------------
	
	public Content() {
		super();
	}
	
	// Variables -------------------------------------
	
	private String title;
	private String description;
	private Date movingDate;
	private String originAddress;
	private GeoCoordinate originCoordinate;
	private String destinationAddress;
	private GeoCoordinate destinationCoordinate;
	private boolean banned;
	
	@NotBlank
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@NotBlank
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@NotNull
	@Valid
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	public Date getMovingDate() {
		return movingDate;
	}
	public void setMovingDate(Date movingDate) {
		this.movingDate = movingDate;
	}
	
	@NotBlank
	public String getOriginAddress() {
		return originAddress;
	}
	public void setOriginAddress(String originAddress) {
		this.originAddress = originAddress;
	}
	
	@Valid
	@AttributeOverrides(value={
		@AttributeOverride(name="latitude", column=@Column(name="originCoordinateLatitude")),
		@AttributeOverride(name="longitude", column=@Column(name="originCoordinateLongitude"))
	})
	public GeoCoordinate getOriginCoordinate() {
		return originCoordinate;
	}
	public void setOriginCoordinate(GeoCoordinate originCoordinate) {
		this.originCoordinate = originCoordinate;
	}
	
	@NotBlank
	public String getDestinationAddress() {
		return destinationAddress;
	}
	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}
	
	@Valid
	@AttributeOverrides(value={
			@AttributeOverride(name="latitude", column=@Column(name="destinationCoordinateLatitude")),
			@AttributeOverride(name="longitude", column=@Column(name="destinationCoordinateLongitude"))
		})
	public GeoCoordinate getDestinationCoordinate() {
		return destinationCoordinate;
	}
	public void setDestinationCoordinate(GeoCoordinate destinationCoordinate) {
		this.destinationCoordinate = destinationCoordinate;
	}
	
	public boolean getBanned() {
		return banned;
	}
	public void setBanned(boolean banned) {
		this.banned = banned;
	}
	
	// Relations ---------------------------------
	
	private Customer customer;
	private Collection<Application> applications;

	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	@Valid
	@OneToMany(mappedBy="content", cascade=CascadeType.REMOVE)
	public Collection<Application> getApplications() {
		return applications;
	}
	public void setApplications(Collection<Application> applications) {
		this.applications = applications;
	}

}
