package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import security.UserAccount;

@Entity
@Access(AccessType.PROPERTY)
public abstract class Actor extends CommentableEntity {
	
	// Constructors -----------------------------------------------------------
	
	public Actor() {
		super();
	}
	
	// Attributes -------------------------------------------------------------
	
	private String name;
	private String surname;
	private String email;
	private String phone;
	
	@NotBlank
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@NotBlank
	@Email
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Pattern(regexp="^(\\+[0-9]{1,3})?\\s?(\\([0-9]{3}\\)\\s)?([a-zA-Z0-9]){4,}(([\\s\\-])([a-zA-Z0-9]){4,})*?$")
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	// Relationships ----------------------------------------------------------
	
	private Collection<Message> sent;
	private Collection<Message> received;
	private UserAccount userAccount;

	@Valid
	@OneToMany(mappedBy="sender", cascade=CascadeType.REMOVE)
	public Collection<Message> getSent() {
		return sent;
	}
	
	public void setSent(Collection<Message> sent) {
		this.sent = sent;
	}
	
	@Valid
	@OneToMany(mappedBy="receiver", cascade=CascadeType.REMOVE)
	public Collection<Message> getReceived() {
		return received;
	}
	
	public void setReceived(Collection<Message> received) {
		this.received = received;
	}
	
	@NotNull
	@Valid
	@OneToOne(cascade=CascadeType.ALL, optional=false)
	public UserAccount getUserAccount() {
		return userAccount;
	}
	
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
}