package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Customer extends Actor {
	
	// Constructors -----------------------------------------------------------
	
	public Customer() {
		super();
	}
	
	// Attributes -------------------------------------------------------------
	
	
	// Relationships ----------------------------------------------------------
	
	private Collection<Content> contents;
	private Collection<Application> applications;
	
	@Valid
	@OneToMany(mappedBy="customer", cascade=CascadeType.REMOVE)
	public Collection<Content> getContents() {
		return contents;
	}
	
	public void setContents(Collection<Content> contents) {
		this.contents = contents;
	}
	
	@Valid
	@OneToMany(mappedBy="customer",cascade=CascadeType.REMOVE)
	public Collection<Application> getApplications() {
		return applications;
	}
	
	public void setApplications(Collection<Application> applications) {
		this.applications = applications;
	}

}