package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Comment extends DomainEntity {
	
	// Constructor --------------------------
	
	public Comment() {
		super();
	}
	
	// Variables ----------------------------
	
	private String title;
	private Date creationDate;
	private String content;
	private int stars;
	private boolean banned;
	
	@NotBlank
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@NotNull
	@Valid
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@NotBlank
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@Range(min=0, max=5)
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	
	public boolean isBanned() {
		return banned;
	}
	public void setBanned(boolean banned) {
		this.banned = banned;
	}
	
	// Relations ----------------------------
	
	private CommentableEntity commentableEntity;
	private Actor actor;

	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public CommentableEntity getCommentableEntity() {
		return commentableEntity;
	}
	public void setCommentableEntity(CommentableEntity commentableEntity) {
		this.commentableEntity = commentableEntity;
	}
	
	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Actor getActor() {
		return actor;
	}
	public void setActor(Actor actor) {
		this.actor = actor;
	}
	
}
