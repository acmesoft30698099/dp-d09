package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Access(AccessType.PROPERTY)
public class Application extends DomainEntity {
	
	// Constructor -----------------------------
	
	public Application() {
		super();
	}
	
	// Variables -------------------------------
	
	private String status;

	@Pattern(regexp="^(ACCEPTED)|(PENDING)|(DENIED)$")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	// Relations -------------------------------
	
	private Customer customer;
	private Content content;

	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

}
