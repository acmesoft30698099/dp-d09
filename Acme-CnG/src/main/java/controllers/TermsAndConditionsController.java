package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/termAndconditions")
public class TermsAndConditionsController extends AbstractController {
	
	// Constructor
	
	public TermsAndConditionsController() {
		super();
	}
	
	// Show
	
	@RequestMapping("/show")
	public ModelAndView show() {
		ModelAndView result;
		
		result = new ModelAndView("termAndconditions/show");
		
		return result;
	}

}
